package lt.vu.mif.jate.tasks.task03.jpa;

import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created on 5/27/16.
 *
 * @author invertisment
 */
public class Shop implements AutoCloseable {
    private final DbManager db;

    public Shop(DbManager db) {
        this.db = db;
    }

    public <T> Set<T> filter(Class<T> leClass, Predicate<T> predicate) {
        return filterInternal(leClass, predicate)
                .collect(Collectors.toSet());
    }

    public <T, M> Set<M> filterAndMap(Class<T> leClass, Predicate<T> predicate, Function<T, M> mapper) {
        return filterInternal(leClass, predicate)
                .map(mapper)
                .collect(Collectors.toSet());
    }

    private <T> Stream<T> filterInternal(Class<T> leClass, Predicate<T> predicate) {
        return db.getListOf(leClass)
                .stream()
                .filter(predicate);
    }

    @Override
    public void close() throws Exception {
        db.close();
    }
}
