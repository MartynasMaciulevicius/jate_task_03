package lt.vu.mif.jate.tasks.task03.jpa.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

import static lt.vu.mif.jate.tasks.task03.jpa.model.Person.PERSON;

/**
 * Created on 5/27/16.
 *
 * @author invertisment
 */
@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@Table(name = "subjects")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorValue(value = PERSON)
public class Person extends Subject {
    protected static final String PERSON = "PERSON";
    @Column(name = "sub_first_name")
    private String firstName;
    @Column(name = "sub_last_name")
    private String lastName;
    @Column(name = "sub_gender")
    @Enumerated(value = EnumType.STRING)
    private Gender gender;

}
