package lt.vu.mif.jate.tasks.task03.jpa.model;

import lombok.Data;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

/**
 * Created on 5/27/16.
 *
 * @author invertisment
 */
@Data
@Entity
@Table(name = "products")
public class Product implements Serializable {

    @Id
    @Column(name = "pro_id")
    Integer id;
    @Column(name = "pro_name")
    String name;
    @Column(name = "pro_brand")
    String brand;
    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "sal_pro_id")
    @LazyCollection(value = LazyCollectionOption.TRUE)
    Collection<Sale> sales;

}
