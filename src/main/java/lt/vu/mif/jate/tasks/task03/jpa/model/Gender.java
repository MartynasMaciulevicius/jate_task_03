package lt.vu.mif.jate.tasks.task03.jpa.model;

import lombok.EqualsAndHashCode;

/**
 * Created on 5/27/16.
 *
 * @author invertisment
 */
public enum Gender {
    M,
    F
}
