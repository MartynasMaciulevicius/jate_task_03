package lt.vu.mif.jate.tasks.task03.jpa;

import lt.vu.mif.jate.tasks.task03.jpa.model.Product;
import lt.vu.mif.jate.tasks.task03.jpa.model.Sale;
import lt.vu.mif.jate.tasks.task03.jpa.model.Subject;

import java.util.Optional;

/**
 * Created on 5/27/16.
 *
 * @author invertisment
 */
public class TableInfo {

    public <T> Optional<String> getTableName(Class<T> clazz) {
        if (clazz.equals(Product.class))
            return Optional.of("products");
        if (clazz.equals(Subject.class))
            return Optional.of("subjects");
        if (clazz.equals(Sale.class))
            return Optional.of("sales");
        return Optional.empty();
    }
}
