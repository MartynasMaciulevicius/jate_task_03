package lt.vu.mif.jate.tasks.task03.jpa.model;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;

/**
 * Created on 5/27/16.
 *
 * @author invertisment
 */
@Data
@Entity
@Table(name = "sales")
public class Sale {

    @Id
    @Column(name = "sal_id")
    Integer id;
    @Column(name = "sal_cost")
    BigDecimal cost;
    @Column(name = "sal_units")
    BigInteger units;
    @Column(name = "sal_time")
    Calendar time;

    @ManyToOne
    @JoinColumn(name = "sal_customer_sub_id")
    Subject customer;

    @ManyToOne
    @JoinColumn(name = "sal_seller_sub_id")
    Subject seller;

    @OneToOne
    @PrimaryKeyJoinColumn(name = "sal_pro_id")
    @Transient
    Product product;

    @Transient
    @ManyToOne
    private Subject subjects;

}
