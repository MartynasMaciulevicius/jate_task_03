package lt.vu.mif.jate.tasks.task03.jpa.model;

import lombok.Data;
import org.hibernate.annotations.DiscriminatorFormula;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

import static lt.vu.mif.jate.tasks.task03.jpa.model.Subject.SUBJECT;

/**
 * Created on 5/27/16.
 *
 * @author invertisment
 */
@Data
@Entity
@Table(name = "subjects")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorFormula("CASE " +
        "WHEN sub_company_title IS NOT NULL THEN '" + Company.COMPANY + "' " +
        "WHEN sub_first_name IS NOT NULL AND sub_last_name IS NOT NULL THEN '" + Person.PERSON + "' " +
        "ELSE '" + SUBJECT + "' end")
@DiscriminatorValue(value = SUBJECT)
public class Subject {
    protected static final String SUBJECT = "SUBJECT";

    @Id
    @Column(name = "sub_id")
    Integer id;
    @Column(name = "sub_address")
    String address;
    @Column(name = "sub_city")
    String city;
    @Column(name = "sub_country")
    String country;
    @Column(name = "sub_county")
    String county;
    @Column(name = "sub_email")
    String email;
    @Column(name = "sub_phone")
    String phone;
    @Column(name = "sub_state")
    String state;
    @Column(name = "sub_web")
    String web;
    @Column(name = "sub_zip")
    String zip;

    @OneToMany(mappedBy = "customer")
    List<Sale> purchases = new ArrayList<>();

    @OneToMany(mappedBy = "seller")
    List<Sale> sales = new ArrayList<>();

}
