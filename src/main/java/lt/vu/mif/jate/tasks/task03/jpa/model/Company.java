package lt.vu.mif.jate.tasks.task03.jpa.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

import static lt.vu.mif.jate.tasks.task03.jpa.model.Company.COMPANY;

/**
 * Created on 5/27/16.
 *
 * @author invertisment
 */
@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@Table(name = "subjects")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorValue(value = COMPANY)
public class Company extends Subject {
    protected static final String COMPANY = "COMPANY";

    @Column(name = "sub_company_title")
    String title;

}
