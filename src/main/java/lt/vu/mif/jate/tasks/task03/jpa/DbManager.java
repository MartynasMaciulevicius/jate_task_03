package lt.vu.mif.jate.tasks.task03.jpa;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Root;
import java.util.List;

public class DbManager implements AutoCloseable {

    private final EntityManagerFactory factory;
    private final EntityManager entityManager;
    private final CriteriaBuilder cb;

    public DbManager() {
        factory = Persistence.createEntityManagerFactory("stud");
        entityManager = factory.createEntityManager();
        cb = entityManager.getCriteriaBuilder();

    }

    @Override
    public void close() throws Exception {
        entityManager.close();
        factory.close();
    }

    public <T> List<T> getListOf(Class<T> leClass) {
        CriteriaQuery<T> cq = cb.createQuery(leClass);
        CriteriaQuery<T> select = cq.select(cq.from(leClass));
        return execForList(select);
    }

    public <T> List<T> getListOf(Class<T> leClass, String orderBy) {
        CriteriaQuery<T> cq = cb.createQuery(leClass);
        Root<T> from = cq.from(leClass);

        Expression<?> expression = from.get(orderBy);
        CriteriaQuery<T> select = cq.select(from)
                .orderBy(cb.asc(expression));

        return execForList(select);
    }

    public <T> T getById(Class<T> leClass, int id) {
        CriteriaQuery<T> cq = cb.createQuery(leClass);
        Root<T> from = cq.from(leClass);
        CriteriaQuery<T> select = cq.select(from)
                .where(cb.equal(from.get("id"), id));   // I'm going to burn in hell for this hardcoded value

        return execForSingle(select);
    }

    private <T> List<T> execForList(CriteriaQuery<T> select) {
        TypedQuery<T> typedQuery = entityManager.createQuery(select);
        return typedQuery.getResultList();
    }

    private <T> T execForSingle(CriteriaQuery<T> select) {
        TypedQuery<T> typedQuery = entityManager.createQuery(select);
        return typedQuery.getSingleResult();
    }

}
