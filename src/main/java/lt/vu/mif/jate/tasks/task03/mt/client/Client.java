package lt.vu.mif.jate.tasks.task03.mt.client;

import lombok.ToString;
import lt.vu.mif.jate.tasks.task03.mt.common.Response;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketException;
import java.nio.ByteBuffer;
import java.util.Optional;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Client object: implements Java API for server-side arithmetic operations protocol.
 * <p>
 * Request (client -> server) message format:
 * <p>
 * - message length (int, 4 bytes): length of the remaining message
 * - request code (int, 4 bytes): message (operation) type, @see lt.vu.mif.jate.tasks.task03.mt.common.Request
 * - correlation id (long, 8 bytes): id to correlate request-response pair
 * - operand1 (long, 8 bytes, optional): first operand
 * - operand2 (long, 8 bytes, optional): second operand
 * <p>
 * Response (server -> client) message format:
 * <p>
 * - message length (int, 4 bytes): length of the remaining message
 * - response code (int, 4 bytes): message type, @see lt.vu.mif.jate.tasks.task03.mt.common.Response
 * - correlation id (long, 8 bytes): id to correlate request-response pair
 * - result (long, 8 bytes, optional): operation result (if code == Success)
 * - message (String, optional): error message (if code == Error)
 *
 * @author valdo
 */
@ToString(of = "id")
public class Client implements AutoCloseable {

    private static final ReentrantLock SOCKET_LOCK = new ReentrantLock();
    private static final int SOCKET_CONNECTION_RETRY_THRESHOLD = 5;

    private static boolean isAnyoneUsingTheSocket;
    private static int allId = 0;

    private final int id;
    private final InetSocketAddress address;
    private final boolean verbose = false;

    private Socket socket;
    private boolean isSocketMine = false;

    public Client(InetSocketAddress address) throws IOException {
        this.address = address;
        id = allId++;

        if (isAnyoneUsingTheSocket)
            throw new IOException(String.format("%s: Multiple connections are not allowed", this));
        isAnyoneUsingTheSocket = true;
        isSocketMine = true;
        socket = new Socket(address.getAddress(), address.getPort());
    }

    public Long addition(Long op1, Long op2) throws ServerFunctionException, IOException {
        return exec(ServerFunction.Addition, op1, op2);
    }

    public Long substraction(Long op1, Long op2) throws ServerFunctionException, IOException {
        return exec(ServerFunction.Substraction, op1, op2);
    }

    public Long multiplication(Long op1, Long op2) throws ServerFunctionException, IOException {
        return exec(ServerFunction.Multiplication, op1, op2);
    }

    public Long division(Long op1, Long op2) throws ServerFunctionException, IOException {
        return exec(ServerFunction.Division, op1, op2);
    }

    public Long function01(Long op1, Long op2) throws ServerFunctionException, IOException {
        return exec(ServerFunction.Function01, op1, op2);
    }

    public Long function02(Long op1, Long op2) throws ServerFunctionException, IOException {
        return exec(ServerFunction.Function02, op1, op2);
    }

    private Long exec(ServerFunction func, Long op1, Long op2) throws ServerFunctionException, IOException {
        return locked(
                SOCKET_LOCK,
                () -> execUnlocked(func, op1, op2));
    }

    private Long execUnlocked(ServerFunction func, Long op1, Long op2) throws ServerFunctionException, IOException {

        int socketRetryCount = 0;
        while (true) {
            if (verbose) System.out.println(String.format("%s %s %s", this, func, socketRetryCount));
            try {

                Optional<Long> maybeLong = doRequest(func, op1, op2, socket);
                if (maybeLong.isPresent())
                    return maybeLong.get();
            } catch (SocketException se) {
                if (verbose)
                    System.out.println(String.format("%s: Trying to recover from *%s*", this, se.getMessage()));
                reopenTheSocket();
//                se.printStackTrace();
                if (socketRetryCount >= SOCKET_CONNECTION_RETRY_THRESHOLD) {
                    break;
                }
                socketRetryCount++;
            }
        }

        throw new IOException(String.format("%s: Server timed out %s times.", this, socketRetryCount));
    }

    private void reopenTheSocket() throws IOException {

        if (verbose) System.out.println(String.format("%s: Reconnection timeout", this));
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (!isSocketMine)
            throw new RuntimeException(String.format("%s: Socket race [reopen]. Thread: %s", this, Thread.currentThread().getName()));

        socket.close();
        socket = new Socket(address.getAddress(), address.getPort());
    }

    private Optional<Long> doRequest(ServerFunction func, Long op1, Long op2, Socket socket) throws ServerFunctionException, IOException {

        OutputStream out = socket.getOutputStream();
        InputStream in = socket.getInputStream();

        //
        // Sending message
        //

        // Building a message and a header
        Message m = new Message(func, op1, op2);
        ByteBuffer bytes = m.toBytes();
        ByteBuffer sizeb = Message.toBytes(bytes.capacity());

        // Write both header and body
        out.write(sizeb.array());
        out.write(bytes.array());
        out.flush();

        //
        // Receiving message back
        //

        // Receive header
        ByteBuffer header = ByteBuffer.allocate(4);
        in.read(header.array());
        header.rewind();

        int headerInt = header.getInt();
        if (headerInt == 0)
            return Optional.empty();
        // Read body length from the header and receive body
        ByteBuffer body = ByteBuffer.allocate(headerInt);
        in.read(body.array());
        body.rewind();

        //
        // Disassemble body and return
        //

        int bodyInt = body.getInt();
        // Response code
        Response resp = Response.fromInt(bodyInt);

        // Correlation ID
        long corr = body.getLong();

        // Check if it is my message
        if (corr == m.getCorrelation()) {
            switch (resp) {

                case Success:

                    // Return result
                    return Optional.of(body.getLong());

                case Error:

                    // Define byte array
                    byte[] mb = new byte[body.remaining()];

                    // Fill array
                    body.get(mb);

                    // Throw exception
                    throw new ServerFunctionException(new String(mb));

            }
        } else {
            throw new IOException("Wrong correlation id received: expected " + m.getCorrelation() + ", got " + corr);
        }

        return Optional.of(0L);
    }

    @Override
    public void close() throws Exception {

        if (!isSocketMine) {
            throw new RuntimeException(String.format("%s: Socket race [close]. Thread: %s", this, Thread.currentThread().getName()));
        }

        socket.close();
        socket = null;
        isAnyoneUsingTheSocket = false;
    }


    private <Ret> Ret locked(ReentrantLock lock, Exec<Ret> exec) throws ServerFunctionException, IOException {
        lock.lock();
        try {
            return exec.exec();
        } finally {
            lock.unlock();
        }
    }

    private interface Exec<Ret> {
        Ret exec() throws ServerFunctionException, IOException;
    }

}